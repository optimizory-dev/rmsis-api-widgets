package com.optimizory.sample.rmsis.api.widgets.api;

public interface MyPluginComponent
{
  String getName();
}