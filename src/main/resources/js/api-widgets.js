AJS.toInit(function() {

  /**
   * On clicking create requirement button new requirement
   * in RMsis is create with issue summary and description
   * and then that newly created requirement is linked with
   * Jira issue in RMsis
   */
  AJS.$('#api-widget-create-requirement').click(function (eve) {
    eve.preventDefault();
    Processor.Requirement();
  });

  /**
   * On clicking create test case button new test case
   * in RMsis is create with issue summary and description
   * and then that newly created test case is linked with
   * Jira issue in RMsis
   */
  AJS.$('#api-widget-create-test-case').click(function (eve) {
    eve.preventDefault();
    Processor.TestCase();
  });

  //On click processor
  var Processor = new function() {
    var self = this;
    var _message = '';

    var _showMessage = function () {
      AJS.flag({
        'type': 'info',
        'body': _message,
        'close': 'auto'
      });
      _message = '';
    };

    var _createLinkSuccess = function () {
      _message += 'and linked successfully with Jira Issue';
      _showMessage();
      JIRA.trigger(JIRA.Events.REFRESH_ISSUE_PAGE, [AJS.Meta.get('issue-key')]);
    };

    var _createLinkFailure = function () {
      _message += 'but not linked with Jira Issue';
      _showMessage();
    };

    var _linkRequirementWithIssue = function (requirementId, issueId) {
      Apis.CreateRelationship(
        'REQUIREMENT_ARTIFACT',
        requirementId,
        issueId,
        _createLinkSuccess,
        _createLinkFailure
      );
    };

    var _linkTestCaseWithIssue = function (testCaseId, issueId) {
      Apis.CreateRelationship(
        'TEST_CASE_ARTIFACT',
        testCaseId,
        issueId,
        _createLinkSuccess,
        _createLinkFailure
      );
    };

    var _createRequirementByIssue = function (issue) {
      Apis.CreateRequirement(issue, function (requirement) {
        _message += 'Created Requirement: <strong>' + requirement.key + '</strong><br />';
        _linkRequirementWithIssue(requirement.id, issue.id);
      });
    };

    _createTestCaseByIssue = function (issue) {
      Apis.CreateTestCase(issue, function (testCase) {
        _message += 'Created Test Case: <strong>' + testCase.key + '</strong><br />';
        _linkTestCaseWithIssue(testCase.id, issue.id);
      });
    };

    self.Requirement = function () {
      _message = '';
      IssueDetails.Fetch(_createRequirementByIssue);
    };

    self.TestCase = function () {
      _message = '';
      IssueDetails.Fetch(_createTestCaseByIssue);
    };
  };

  /**
   * Collection of RMsis GraphQL API
   */
  var Apis = new function() {
    //GraphQL Endpoint
    var _baseURL = contextPath + '/rest/service/latest/rmsis/graphql';
    var _graphql = graphql(_baseURL, {
      method: "POST",
      asJSON: true
    });

    var _processErrors = function (errors, failure) {
      var error = '';
      if (AJS.$.isArray(errors)) {
        AJS.$.each(errors, function (k, v) {
          error += v.message + "\n";
        });
      } else {
        error = errors;
      }

      AJS.flag({
        'type': 'error',
        'body': error,
        'close': 'auto'
      });

      if (typeof failure === "function") {
        failure();
      }
    };

    //Fetches RMsis ID of Jira issue
    this.FetchArtifact = function (id, success, failure) {
      var query = 'query ($externalId: String!) {' + "\n" +
                  '  getArtifactByExternalId(externalId: $externalId) {' + "\n" +
                  '    id' + "\n" +
                  '  }' + "\n" +
                  '}';

      _graphql(query, {
        'externalId': id
      }).then(function (response) {
        success(response.getArtifactByExternalId.id);
      }).catch(function (errors) {
        _processErrors(errors, failure);
      });
    };

    //Fetches RMsis ID of Jira project
    this.FetchProject = function (id, success, failure) {
      var query = 'query ($externalId: String!) {' + "\n" +
                  '  getProjectByExternalId(externalId: $externalId) {' + "\n" +
                  '    id' + "\n" +
                  '  }' + "\n" + 
                  '}';

      _graphql(query, {
        'externalId': id
      }).then(function (response) {
        success(response.getProjectByExternalId.id);
      }).catch(function (errors) {
        _processErrors(errors, failure);
      });
    };


    //Create RMsis requirement
    this.CreateRequirement = function (issue, success, failure) {
      var query = 'mutation ($projectId: Long!, $summary: String!, $description: String) {' + "\n" +
                  '  createPlannedRequirement(projectId: $projectId, summary: $summary, description: $description) {' + "\n" +
                  '    id' + "\n" +
                  '    key' + "\n" +
                  '  }' + "\n" +
                  '}';

      _graphql(query, {
        'projectId': issue.project,
        'summary': issue.summary,
        'description': issue.description
      }).then(function (response) {
        success(response.createPlannedRequirement);
      }).catch(function (errors) {
        _processErrors(errors, failure);
      });
    };

    this.CreateTestCase = function (issue, success, failure) {
      var query = 'mutation ($projectId: Long!, $name: StringMin0Max255, $description: String) {' + "\n" +
                  '  createTestCase(projectId: $projectId, testCase: {name: $name, description: $description}) {' + "\n" +
                  '    id' + "\n" +
                  '    key' + "\n" +
                  '  }' + "\n" +
                  '}';

      _graphql(query, {
        'projectId': issue.project,
        'name': issue.summary,
        'description': issue.description
      }).then(function (response) {
        success(response.createTestCase);
      }).catch(function (errors) {
        _processErrors(errors, failure);
      });
    };

    this.CreateRelationship = function (name, source, target, success, failure) {
      var query = 'mutation ($name: RelationshipEnum!, $sourceId: Long!, $targetId: Long!) {' + "\n" +
                  '  createRelationship(name: $name, sourceId: $sourceId, targetId:$targetId)' + "\n" +
                  '}';

      _graphql(query, {
        'name': name,
        'sourceId': source,
        'targetId': target
      }).then(function (response) {
        success();
      }).catch(function (errors) {
        _processErrors(errors, failure);
      });
    };
  };

  /**
   * Process issue details using current issue key uses:
   * Jira Server API - https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/
   * RMsis GraphQL API
   */
  var IssueDetails = new function () {
    var self = this,
        _key = AJS.Meta.get('issue-key'),
        _fields = encodeURIComponent('summary,description,project'),
        _url = contextPath + '/rest/api/latest/issue/' + _key + '?fields=' + _fields,
        _inProgress = false;

    self.callback = {
      failure: function () {
      }
    };

    var _callbackFailure = function () {
      _inProgress = false;
      self.callback.failure();
    };

    var _callbackSuccess = function () {
      _inProgress = false;
      self.callback.success(self.issue);
    };

    var _getRMsisProject = function () {
      Apis.FetchProject(self.issue.project, function (id) {
        self.issue.project = id;
        _callbackSuccess();
      }, _callbackFailure);
    };

    var _getRMsisIssue = function () {
      Apis.FetchArtifact(self.issue.id, function (id) {
        self.issue.id = id;
        _getRMsisProject();
      }, _callbackFailure);
    };

    var _getJiraIssue = function () {
      AJS.$.getJSON(_url, function (data) {
        self.issue = {
          id: data.id,
          summary: data.fields.summary,
          description: data.fields.description,
          project: data.fields.project.id
        };

        _getRMsisIssue();
      }).error(function (jqXHR, textStatus, errorThrown) {
        var error = AJS.$.parseJSON(jqXHR.responseText);
        error = error ? error.errorMessages : 'Error fetching Jira issue';
        AJS.flag({
          'type': 'error',
          'body': error,
          'close': 'auto'
        });
        _callbackFailure();
      });
    }


    self.Fetch = function (success) {
      if (_inProgress) {
        AJS.flag({
          'type': 'warning',
          'body': 'Please wait, issue fetch already in progress',
          'close': 'auto'
        });

        return;
      }
      _inProgress = true;
      self.callback.success = success;
      if (self.issue) {
        _callbackSuccess();
      } else {
        _getJiraIssue();
      }
    };
  };
});
