# RMsis API Widget

[![N|Solid](https://www.optimizory.com/wp-content/themes/arras-theme/images/optimizory_logo.png)](https://www.optimizory.com)

This plugin is used to demostrate usage of RMsis GraphQL API's.

# Feature List
  - Uses *entity create api* to push Jira issue summary and descrption as requirement and test case in RMsis
  - Uses *relationship link api* to link newly created requirement or test case to Jira issue 

# Installation
  As this plugin based on Atlassian SDK you refer to [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK)

# Support
 - This plugin is not supported
 - But for queries related to RMsis api usage, you can communicate with us at [Optimiozry Support](mailto:support@optimizory.com)
